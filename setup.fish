#!/usr/bin/fish

if not type -q fisher
    curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
end

fisher install jorgebucaran/autopair.fish
fisher install jethrokuan/z
fisher install nickeb96/puffer-fish
fisher install IlanCosman/tide

# tide configure
echo "if string match -v 'xterm-*color' \$TERM >/dev/null
    set -g tide_character_icon '>'
    exit 0
end" > ~/.config/fish/conf.d/tide_vars.fish
curl -sL https://codeberg.org/buzz-tee/basic-fish/raw/branch/main/tide_vars \
    | sed -E 's/^SETUVAR\s+([^:]+):/set -g \1 /g' \
    | sed 's/\\\\x1d//g' \
    | sed 's/\\\\x1e/ /g' \
    | sed 's/\\\\x20/" "/g' \
    | sed 's/\\\\x25/%/g' \
    | sed 's/\\\\x2d/-/g' \
    | sed 's/\\\\x2e/./g' \
    >> ~/.config/fish/conf.d/tide_vars.fish

if type -q python3
    python3 -m pip install virtualfish
else
    python -m pip install virtualfish
end

set -U fish_user_paths ~/.local/bin $fish_user_paths

vf install auto_activation projects

echo "abbr --add --global ll exa --long --group --header --git --time-style long-iso  --all --group-directories-first
abbr --add --global lt exa --long --group --all --git --tree" > ~/.config/fish/conf.d/exa.fish

echo "set -g fish_user_paths ~/.local/bin $fish_user_paths
set -g PROJECT_HOME ~/Projekte" > ~/.config/fish/conf.d/paths.fish

echo "function fish_greeting
    set os (uname -a)
    echo \"fish \$FISH_VERSION (pid=\$fish_pid) @ \$os\"
end" > ~/.config/fish/functions/fish_greeting.fish

echo "function fish_title
    # emacs' "term" is basically the only term that can't handle it.
    if not set -q INSIDE_EMACS; or string match -vq '*,term:*' -- \$INSIDE_EMACS
        # An override for the current command is passed as the first parameter.
        # This is used by `fg` to show the true process name, among others.
        echo (set -q argv[1] && echo \$argv[1] || status current-command)
        echo ' ('
        if set -q SUDO_USER
            echo (id -un)'>'
        end
        if set -q SSH_CLIENT 
            echo (id -un)'@'(hostname)':'
        else if pstree -sZA \$fish_pid | grep -e '^\s*`-sshd(`\')\$' > /dev/null
            pstree -nsuZA \$fish_pid | grep -e '^\s*`-sshd([^,]*,`\')\$' | sed 's/\s*`-sshd(\([^,]*\).*/\1/'
            echo '@'(hostname)':'
        end
        echo (__fish_pwd)')'
    end
end" > ~/.config/fish/functions/fish_title.fish

echo "function posix-source
	for i in (cat \$argv)
		set arr (echo \$i |tr = \n)
  		set -gx \$arr[1] \$arr[2]
	end
end" > ~/.config/fish/functions/posix-source.fish

exec fish

